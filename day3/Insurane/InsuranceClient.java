public class InsuranceClient {
    public static void main(String[] args) {
        String insuranceCompany =args[0];
        Insurance insurance=null;

        switch (insuranceCompany){
            case "1":
                BajajInsurance bajajInsurance=new BajajInsurance();
                insurance=bajajInsurance;
                break;

            case "2":
                TataIG tataIG=new TataIG();
                insurance=tataIG;
                break;
            default:
                System.out.println("Enter valid Firm");
                break;
        }
        SavingsAccount customer= new SavingsAccount("Navin", 20_00_000);
        double premium= insurance.calculatePremium("Ritz", "2016", 6_00_000);
        if(premium<customer.accountBalance){
            System.out.println("Amount After Deducted: "+ customer.withdraw(premium));
            String policyNumber=insurance.payPremium(premium, "45862", "2016");
            System.out.println("Policy Number: "+ policyNumber);
        }
        else{
            System.out.println("Insufficient Funds");
        }
    }
}
