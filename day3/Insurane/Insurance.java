public interface Insurance{
    static long policyId=10000;
    double calculatePremium(String vehicleName, String vehicleModel, double amount);
    String payPremium(double premiumAmount, String vehicleNumber, String vehicleModel);

}

class SavingsAccount{

    private static long accountNumberTracker=10_000;

    private long accountNumber = accountNumberTracker;
    public String customerName;
    public static double accountBalance;

    public SavingsAccount(String customerName, double accountBalance){
        this.accountNumber = ++accountNumberTracker;
        this.customerName = customerName;
        this.accountBalance = accountBalance;
    }
    public double withdraw(double amount){
        if(this.accountBalance-amount>=0) {
            this.accountBalance-=this.accountBalance-amount;
            return this.accountBalance;
        }else{
            return 0;
        }
    }
    public double deposit(double amount){
        this.accountBalance+=amount;
        return this.accountBalance;
    }
}

class BajajInsurance implements Insurance{
    private static long policytrackingIdBajaj= 10000;
    public double calculatePremium(String vehicleName, String vehicleModel, double amount){
        double premium=amount*1.12;
        System.out.println(premium);
        return premium;
    }
    public String payPremium(double premiumAmount, String vehicleNumber, String vehicleModel){
        String policyId="BI"+policytrackingIdBajaj;
        policytrackingIdBajaj++;
        return policyId;
    }
}
class TataIG implements Insurance{
    private static long policytrackingIdTata=20000;
    public double calculatePremium(String vehicleName, String vehicleModel, double amount){
        double premium=amount*1.15;
        return premium;
    }
    public String payPremium(double premiumAmount, String vehicleNumber, String vehicleModel){
        String policyId="BI"+policytrackingIdTata;
        policytrackingIdTata++;
        return policyId;
    }
}


