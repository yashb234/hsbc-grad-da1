interface PaymentGateway {
    
    void doPayment(String from, String to, double amount, String notes);

}

interface MobileRecharge {

    void phoneRecharge(String phonenumber, double amount);

}

class GooglePay implements PaymentGateway, MobileRecharge {
    public void doPayment(String from, String to, double amount, String notes) {
        System.out.println("Payment from " + from + " to: " + to + "of Amount " + amount + " Notes : " + notes
                + " Using Google Pay");
    }

    public void phoneRecharge(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + "with GooglePay ");
    }

}

class PhonePay implements PaymentGateway, MobileRecharge {
    public void doPayment(String from, String to, double amount, String notes) {
        System.out.println("Payment from " + from + " to: " + to + "of Amount " + amount + " Notes : " + notes
                + " Using Phone Pay");
    }

    public void phoneRecharge(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + " with PhonePay");
    }
}

class JioPay implements PaymentGateway {
    public void doPayment(String from, String to, double amount, String notes) {
        System.out.println(
                "Payment from " + from + " to: " + to + "of Amount " + amount + " Notes : " + notes + " Using Jio Pay");
    }
}

class BharatPay implements PaymentGateway, MobileRecharge {
    public void doPayment(String from, String to, double amount, String notes) {
        System.out.println("Payment from " + from + " to: " + to + "of Amount " + amount + " Notes : " + notes
                + " Using BharatPay");
    }

    public void phoneRecharge(String phonenumber, double amount) {
        System.out.println("Mobile recharge for " + phonenumber + " with amount  " + amount + "with BharatPay ");
    }

}

public class PaymentGateWayClient {
    public static void main(String[] args) {

        PaymentGateway paymentGateway = null;
        MobileRecharge mobileRecharge = null;
        if (args[0] == "1") {
            GooglePay googlePay = new GooglePay();
            paymentGateway = googlePay;
            mobileRecharge = googlePay;
        } else if(args[0] == "2"){
            PhonePay phonePay = new PhonePay();
            paymentGateway = phonePay;
            mobileRecharge = phonePay;
        }else{
            BharatPay bharatPay= new BharatPay();
            paymentGateway = bharatPay;
            mobileRecharge = bharatPay;

        }
        paymentGateway.doPayment("Amir", "carry", 200, "Please confirm");
        mobileRecharge.phoneRecharge("9865285746", 200);
    }
}
