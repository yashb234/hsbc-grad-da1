abstract class BankAccounts {

    private static long accountNumberTracker=10_000;

    //Mandatory
    private long accountNumber = accountNumberTracker;
    public String customerName;

    //Optional
    private  double accountBalance;

    public BankAccounts(String customerName, double accountBalance){
        this.accountNumber = ++accountNumberTracker;
        this.customerName = customerName;
        this.accountBalance = accountBalance;
    }


    abstract double getLoanAmount(double amount);

    public double deposit(double amount){
        this.accountBalance+=amount;
        return this.accountBalance;
    }
    public double withdraw(double amount){
        if(this.accountBalance-amount>=0) {
            return this.accountBalance;
        }else{
            return 0;
        }
    }
    public final double getAccountBalance() {
        return this.accountBalance;
    }

}

final class CurrentAccount extends BankAccounts{

    private String GSTNumber;
    private final long LOAN_ELIGIBILITY = 25_00_000;

    public CurrentAccount(double balance, String customerName, String GSTNumber) {
        super(customerName, balance);
        this.GSTNumber = GSTNumber;
        super.customerName = customerName;
    }


    public final double getLoanAmount(double loanAmount) {
        // validate the loan eligibility
        if(loanAmount<=loanEligibility()) {
            return loanAmount;
        }else{
            return 0;
        }
    }

    public final double loanEligibility() {
        return LOAN_ELIGIBILITY;
    }

}

final class SavingsAccount extends BankAccounts{
    private final long LOAN_ELIGIBILITY = 5_00_000;
    public SavingsAccount(String customerName, double balance) {
        super(customerName, balance);
        super.customerName = customerName;
    }

    public final double getLoanAmount(double loanAmount) {
        // validate the loan eligibility
        if(loanAmount<=loanEligibility()) {
            return loanAmount;
        }else{
            return 0;
        }
    }

    public final double loanEligibility() {
        return LOAN_ELIGIBILITY;
    }

}

final class SalariedAccount extends BankAccounts{
    private final long LOAN_ELIGIBILITY = 10_00_000;
    public SalariedAccount(String customerName, double balance) {
        super(customerName, balance);
        super.customerName = customerName;
    }

    public final double getLoanAmount(double loanAmount) {
        // validate the loan eligibility
        if(loanAmount<=loanEligibility()) {
            return loanAmount;
        }else{
            return 0;
        }
    }

    public final double loanEligibility() {
        return LOAN_ELIGIBILITY;
    }
}

