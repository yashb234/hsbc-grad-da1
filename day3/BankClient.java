public class BankClient {
    public static void main(String[] args) {
        BankAccounts accountType=null;

        String accountId =args[0];
        switch (accountId){
            case ("1"):
                accountType= new CurrentAccount(60_000, "Amar","hjb67vds");
                break;
            case ("2"):
                accountType= new SavingsAccount("Akbar",45_000);
                break;
            case ("3"):
                accountType= new SalariedAccount("Anthony",60_000);
                break;
            default:
                System.out.println("Enter valid value");
                break;
        }

        execute(accountType);
    }
    public static void execute(BankAccounts accountType){
        accountType.deposit(50_000);
        accountType.getLoanAmount(20_00_000);
    }
}
