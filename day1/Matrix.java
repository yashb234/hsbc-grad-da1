public class Matrix {
    public static void main(String[] args) {
        int row=5;
        int col=5;
        int[][] matrix= new int[row][col];

        assignMatrix(matrix);
        printMatrix(matrix);
    }
    public static void assignMatrix(int[][] matrix){
        int intValue=10;
        for(int rowIndex=0; rowIndex<5;rowIndex++){
            for(int colIndex=0; colIndex<5;colIndex++){
                matrix[rowIndex][colIndex]=intValue++;
            }
        }
    }
    public static void printMatrix(int[][] matrix){
        for(int rowIndex=0; rowIndex<5;rowIndex++){
            for(int colIndex=0; colIndex<5;colIndex++){
                System.out.print("\t"+matrix[rowIndex][colIndex]+"\t");
            }
            System.out.println();
        }
    }
}
