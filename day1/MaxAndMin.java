import java.util.Arrays;

public class MaxAndMin {
    public static void main(String[] args) {
        int[] values=new int[5];
        int minValue=Integer.parseInt(args[0]);
        int maxValue=Integer.parseInt(args[1]);
        for(int index=0;index<5;index++){
            int currValue=Integer.parseInt(args[index]);
            if(minValue>currValue){
                minValue=currValue;
            }
            else if(maxValue<Integer.parseInt(args[index])){
                maxValue=currValue;
            }
        }
        System.out.println("Minimum Value: "+ minValue);
        System.out.println("Maximum Value: "+ maxValue);
    }
}
