public class WeekdayOrWeekend {
    public static void main(String[] args) {
        String day = args[0].toUpperCase();
        switch (day) {
            case "SUNDAY":
                System.out.println("It's a Weekend !!");
                break;
            case "SATURDAY":
                System.out.println("It's a Weekend !!");
                break;
            default:
                System.out.println("It's a Weekday");
                break;
        }
    }
}
