public class VowelOrConsonant {
    public static void main(String[] args) {
        String alphaString=args[0].toUpperCase();
        char alphaChar=alphaString.charAt(0);

        switch (alphaChar){
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                System.out.println("It's a Vowel !!");
                break;
            default:
                System.out.println("It's a Constant");
                break;
        }

    }
}
