public class BillStateWise {
    public static void main(String[] args) {
        String valueString=args[0];
        double valueDouble= Float.valueOf(valueString);

        String stateCode=args[1].toUpperCase();
        double totalBill;
        switch (stateCode){
            case "KA":
                totalBill=valueDouble*1.15;
                break;
            case "TN":
                totalBill=valueDouble*1.18;
                break;
            case "MH":
                totalBill=valueDouble*1.20;
                break;
            default:
                totalBill=valueDouble*1.12;
        }
        System.out.println("Value After Tax: "+totalBill);

    }
}
