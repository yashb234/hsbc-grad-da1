package com.hsbc.da1.model;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {
//	public static void main(String[] args) {
//		SavingsAccount Deepa= new SavingsAccount("Deepa", 30_000);
//		SavingsAccount Meepa= new SavingsAccount("Meepa", 40_000);
//		SavingsAccount Neepa= new SavingsAccount("Neepa", 50_000);
//		SavingsAccount Keepa= new SavingsAccount("Keepa", 60_000);
//		
//		Set<SavingsAccount> set = new TreeSet<>(new SortByAccountNameAsc());
//		set.add(Deepa);
//		set.add(Meepa);
//		set.add(Neepa);
//		set.add(Keepa);
//		
//		Iterator<SavingsAccount> it = set.iterator();
//		while(it.hasNext()) {
//			System.out.println(it.next());
//		}
//	} 
//}
//
//class SortByAccountNumberAsc implements Comparator<SavingsAccount>{
//
//	@Override
//	public int compare(SavingsAccount arg0, SavingsAccount arg1) {
//		// TODO Auto-generated method stub
//		return (int) (arg0.getAccountNumber() - arg1.getAccountNumber())  ;
//	}
//
//}
//class SortByAccountNumberDsc implements Comparator<SavingsAccount>{
//
//	@Override
//	public int compare(SavingsAccount arg0, SavingsAccount arg1) {
//		// TODO Auto-generated method stub
//		return -1*(int) (arg0.getAccountNumber() - arg1.getAccountNumber())  ;
//	}
//
//} 
//
//class SortByAccountNameAsc implements Comparator<SavingsAccount>{
//
//	@Override
//	public int compare(SavingsAccount arg0, SavingsAccount arg1) {
//		// TODO Auto-generated method stub
//		return arg0.getCustomerName().compareTo(arg1.getCustomerName());
//	}
//
//}
//class SortByAccountNameDsc implements Comparator<SavingsAccount>{
//
//	@Override
//	public int compare(SavingsAccount arg0, SavingsAccount arg1) {
//		// TODO Auto-generated method stub
//		return arg1.getCustomerName().compareTo(arg0.getCustomerName());
//	}

}