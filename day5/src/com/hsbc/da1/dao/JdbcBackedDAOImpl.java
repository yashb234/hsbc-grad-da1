//$host="localhost";
//$port=3306;
//$socket="";
//$user="root";
//$password="";
//$dbname="mydb";


package com.hsbc.da1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class JdbcBackedDAOImpl implements SavingsAccountDAO {

	private static String connectString = "jdbc:mysql://localhost:3306/mydb ";
	private static String username = "root";
	private static String password = "root";

	private static final String INSERT_QUERY = "insert into savings_account (account_nu mber, cust_name, account_balance)"
			+ " values ";

	private static final String INSERT_QUERY_PREP_STMT = "insert into savings_account (cust_name, account_balance, email_address)"
			+ " values ( ?, ?, ?)";

	private static final String SELECT_QUERY = "select * from savings_account";

	private static final String DELETE_BY_ID_QUERY = "delete  from savings_account where account_number=";

	private static final String SELECT_BY_ID_QUERY = "select *  from savings_account where account_number=";

	private static final String SELECT_BY_EMAIL_QUERY = "select *  from savings_account where email_address= ?";

	private static Statement getStatement() {
		try {
			Connection connection = DriverManager.getConnection(connectString, username, password);
			return connection.createStatement();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Connection getDBConnection() {
		try {
			Connection connection = DriverManager.getConnection(connectString, username, password);
			return connection;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {

		/*
		 * String query = INSERT_QUERY + " (" + savingsAccount.getAccountNumber() + ", "
		 * +"'"+ savingsAccount.getCustomerName()+"'" + "," +
		 * savingsAccount.getAccountBalance() + ")";
		 */
		int numberOfRecordsUpdated = 0;
		try (PreparedStatement pStmt = getDBConnection().prepareStatement(INSERT_QUERY_PREP_STMT);) {
			pStmt.setString(1, savingsAccount.getCustomerName());
			pStmt.setDouble(2, savingsAccount.getAccountBalance());
			pStmt.setString(3, savingsAccount.getEmailAddress());

			numberOfRecordsUpdated = pStmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (numberOfRecordsUpdated == 1) {
			// fetch the newly saved savings account from the database and return the
			// result.
			try {
				SavingsAccount fetchedSavingsAccount = fetchSavingsAccountByEmailAddress(
						savingsAccount.getEmailAddress());
				if (fetchedSavingsAccount != null) {
					return fetchedSavingsAccount;
				}

			} catch (CustomerNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		// TODO
		return null;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		String query = DELETE_BY_ID_QUERY + accountNumber;
		try {
			getStatement().executeUpdate(query);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Collection<SavingsAccount> fetchSavingsAccounts() {
		String query = SELECT_QUERY;
		Set<SavingsAccount> savingsAccountSet = new HashSet<>();
		try {
			ResultSet rs = getStatement().executeQuery(query);
			if (rs.next()) {
				SavingsAccount savingsAccount = new SavingsAccount(rs.getInt("account_number"),
						rs.getString("cust_name"), rs.getDouble("account_balance"), rs.getString("email_address"));
				savingsAccountSet.add(savingsAccount);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return savingsAccountSet;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		String query = SELECT_BY_ID_QUERY + " " + accountNumber;
		try {
			ResultSet rs = getStatement().executeQuery(query);
			if (rs.next()) {
				SavingsAccount savingsAccount = new SavingsAccount(rs.getInt("account_number"),
						rs.getString("cust_name"), rs.getDouble("account_balance"), rs.getString("email_address"));
				return savingsAccount;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new CustomerNotFoundException(" Customer with " + accountNumber + " does not exists");
		}
		return null;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByEmailAddress(String emailAddress) throws CustomerNotFoundException {
		
		try {
			Connection connection = getDBConnection();
			PreparedStatement ps = connection.prepareStatement(SELECT_BY_EMAIL_QUERY);
			ps.setString(1, emailAddress);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				System.out.println("Account number: "+rs.getInt("account_number"));
				
				SavingsAccount savingsAccount = new SavingsAccount(rs.getInt("account_number"),
						rs.getString("cust_name"), rs.getDouble("account_balance"), rs.getString("email_address"));
				System.out.println("Before returning the savings account back to the customer ");
				System.out.println(savingsAccount);
				return savingsAccount;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new CustomerNotFoundException(" Customer with " + emailAddress + " does not exists");
		}
		return null;
	}

}
