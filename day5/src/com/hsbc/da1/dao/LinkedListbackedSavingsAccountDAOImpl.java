package com.hsbc.da1.dao;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class LinkedListbackedSavingsAccountDAOImpl implements SavingsAccountDAO {
	private LinkedList<SavingsAccount> sa=new LinkedList<>();

	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		sa.add(savingsAccount);
		
		return savingsAccount;
	}

	public List<SavingsAccount> fetchAllAccounts() {
		// TODO Auto-generated method stub
		return sa;
	}

	public SavingsAccount fetchAccountById(long accountId) throws CustomerNotFoundException {
		for(SavingsAccount savingsAccount :sa) {
			if(savingsAccount.getAccountNumber()==accountId) {
				return savingsAccount;
			}
		}
		// TODO Auto-generated method stub
		throw new CustomerNotFoundException("Account not found");
	}

	public void updateSavingsAccount1(long accountId, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		for(SavingsAccount savings:sa) {
			if(savings.getAccountNumber()==accountId) {
				savings=savingsAccount;
			}
		}
		
		
		
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		for(SavingsAccount savingsAccount :sa) {
			if(savingsAccount.getAccountNumber()==accountNumber) {
				sa.remove(savingsAccount);
			}
		}
	}

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByEmailAddress(String emailAddress) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}
	

}
