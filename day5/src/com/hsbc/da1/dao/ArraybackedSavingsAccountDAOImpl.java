package com.hsbc.da1.dao;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFoundException;
import com.hsbc.da1.model.SavingsAccount;

public class ArraybackedSavingsAccountDAOImpl implements SavingsAccountDAO{
	private static SavingsAccount[] savingsAccounts=new SavingsAccount[100];
	private static int counter;
	
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		savingsAccounts[counter++]=savingsAccount;
		return savingsAccount;
		
		
	}
	public List <SavingsAccount> fetchAllAccounts() {
		return Arrays.asList(savingsAccounts);		
		
	}
	public SavingsAccount fetchAccountById(long accountId)throws CustomerNotFoundException {
		
		for(int index=0;index<savingsAccounts.length;index++) {
			if(savingsAccounts[index]!=null && savingsAccounts[index].getAccountNumber()==accountId) {
				return savingsAccounts[index];
			}
		}
		throw new  CustomerNotFoundException("account not found");
	}
	
	
	public void updateSavingsAccount1(long accountId,SavingsAccount savingsAccount) {
		for(int index=0;index<savingsAccounts.length;index++) {
			if(savingsAccounts[index].getAccountNumber()==accountId) {
				
				savingsAccounts[index]=savingsAccount;
				break;
			}
			
			
		}
		return;
		
		
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		for(int index=0;index<savingsAccounts.length;index++) {
			if(savingsAccounts[index].getAccountNumber()==accountNumber) {
				savingsAccounts[index]=null;
			}
			break;
		}
		
	}
	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Collection<SavingsAccount> fetchSavingsAccounts() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public SavingsAccount fetchSavingsAccountByEmailAddress(String emailAddress) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	

}