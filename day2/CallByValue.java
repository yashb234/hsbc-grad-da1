public class CallByValue {
    public static void main(String[] args) {
        int operand= 5;
        System.out.println(operand);

        callByValue(operand);

        System.out.println(operand);
    }
    public static void callByValue(int value){
        value=value*2;
        System.out.println(value);
    }
}
