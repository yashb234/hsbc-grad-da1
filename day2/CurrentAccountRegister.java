public class CurrentAccountRegister {
    private static CurrentAccount currentAccount[] = new CurrentAccount[]{
           new CurrentAccount("AGK76DSJBK", "Ramesh", "Dance_Academy"),
           new CurrentAccount("HGJ76DSJBK", "navin", "Technology_Academy", new Address("8th Ave", "Bangalore", "Karnataka", 577142))
    };

    public static CurrentAccount fetchCurrentAccountByuserAccountId(long userAccountId){
        for(CurrentAccount userId : currentAccount){
            if(userId.getAccountNumber()==userAccountId){
                return userId;
            }
        }
        return null;
    }

}
