public class CurrentAccountClient {
    public static void main(String[] args) {

        CurrentAccount ramesh = new CurrentAccount("AGK76DSJBK", "Ramesh", "Dance_Academy");
        System.out.println("Account Holder Name: "+ ramesh.getCostumerName());
        System.out.println("Account NUmber: "+ ramesh.getAccountNumber());
        System.out.println("GST Number: "+ ramesh.getGSTNumber());
        System.out.println("Business Name: "+ ramesh.getBusinessName());
        System.out.println("Accout Balance before Deposit: "+ ramesh.getAccountBalance());
        System.out.println("Account Balance: "+ ramesh.deposit(80_000));
        System.out.println("Amount Withdrawn: "+ramesh.withdraw(5000));
        System.out.println("Account Balance after Withdraw: "+ramesh.getAccountBalance());
        System.out.println();

        Address address = new Address("8th Ave", "Bangalore", "Karnataka", 577142);
        CurrentAccount navin = new CurrentAccount("HGJ76DSJBK", "navin", "Technology_Academy", address);
        System.out.println("Account Holder Name: "+ navin.getCostumerName());
        System.out.println("Account NUmber: "+ navin.getAccountNumber());
        System.out.println("GST Number: "+ navin.getGSTNumber());
        System.out.println("Business Name: "+ navin.getBusinessName());
        System.out.println(navin.getAddress());
        System.out.println("Accout Balance before Deposit: "+ navin.getAccountBalance());
        System.out.println("Account Balance: "+ navin.deposit(60_000));
        System.out.println("Amount Withdrawn: "+ navin.withdraw(40_000));
        System.out.println("Account Balance after Withdraw: "+navin.getAccountBalance());

    }
}
