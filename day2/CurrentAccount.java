public class CurrentAccount {

    private static long accountNumberTracker=10_000;

    //Mandatory
    private long accountNumber = accountNumberTracker;
    private String costumerName;
    private String GSTNumber;
    private String businessName;

    //Optional
    private double accountBalance;
    private Address address;

    public long getAccountNumber(){
        return this.accountNumber;
    }

    public String getCostumerName(){
        return this.costumerName;
    }

    public String getGSTNumber(){
        return this.GSTNumber;
    }

    public String getBusinessName(){
        return this.businessName;
    }

    public double getAccountBalance(){
        return this.accountBalance;
    }

    public String getAddress(){
        return this.address.getFullAddress();
    }

    public double withdraw(double amount){
        if(this.accountBalance-50_000>=50_000) {
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double deposit(double amount){
        this.accountBalance=this.accountBalance+amount;
        return accountBalance;
    }

    public CurrentAccount(String GSTNumber, String costumerName, String businessName){
        this.accountNumber= ++accountNumberTracker;
        this.GSTNumber=GSTNumber;
        this.costumerName=costumerName;
        this.businessName=businessName;
        this.accountBalance=50_000;
    }

    public CurrentAccount(String GSTNumber, String costumerName, String businessName, Address address){
        this.accountNumber= ++accountNumberTracker;
        this.GSTNumber=GSTNumber;
        this.costumerName=costumerName;
        this.businessName=businessName;
        this.accountBalance=50_000;
        this.address=address;

    }


}
