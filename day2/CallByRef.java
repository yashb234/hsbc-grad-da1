public class CallByRef {
    public static void main(String[] args) {
        int[] array=new int[]{1,2,3};

        System.out.println("Before calling");
        for (int value: array){
            System.out.print(value+"\t");
        }
        System.out.println();
        callByRef(array);

        System.out.println("After Calling");
        for (int value: array){
            System.out.print(value+"\t");
        }
    }

    public static void callByRef(int[] arr){
        arr[0]=4;
        arr[1]=5;
        arr[2]=6;
        System.out.println("In Method");
        for (int value: arr){
            System.out.print(value + "\t");
        }
        System.out.println();
    }
}
