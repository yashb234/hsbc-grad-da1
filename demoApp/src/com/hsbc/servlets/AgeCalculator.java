package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Formatter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AgeCalculator extends HttpServlet{
	
	@Override
	public void doGet(HttpServletRequest httpServletReq, HttpServletResponse httpServletRes) throws IOException {
			

			String doStr=httpServletReq.getParameter("dob");
			String arr[]=doStr.split("-");
			
			LocalDate today=LocalDate.now();
			LocalDate dob=LocalDate.of(Integer.parseInt(arr[2]),Integer.parseInt(arr[1]),Integer.parseInt(arr[0]));
			
			Period period=Period.between(dob, today);
			
			
			PrintWriter out = httpServletRes.getWriter();
			
			out.write("<h1> Years: " +period.getYears()+" </h1>");
			out.write("<h1> Months: " +period.getMonths()+" </h1>");
			out.write("<h1> Days: " +period.getDays()+" </h1>");
		}

}
