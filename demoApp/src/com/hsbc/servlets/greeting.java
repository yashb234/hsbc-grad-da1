package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class greeting extends HttpServlet{
	
	@Override
	public void doGet(HttpServletRequest httpServletReq, HttpServletResponse httpServletRes) throws IOException {
		
		String firstname=httpServletReq.getParameter("firstname");
		String lastname=httpServletReq.getParameter("lastname");
		
		PrintWriter out = httpServletRes.getWriter();
		
		out.write("<h1> Hello: " + firstname + " " +  lastname + "  </h1>");
	}

}
