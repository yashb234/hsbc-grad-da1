package com.hsbc.da1.util;

import com.hsbc.da1.dao.ArrayBackedSavingsAccountDAOimpl;
import com.hsbc.da1.dao.SavingsAccountDAO;

public class SavingsAccountDAOFactory {
	
	public static SavingsAccountDAO getSavingsAccountDAO() {
			return new ArrayBackedSavingsAccountDAOimpl();
	}

}
