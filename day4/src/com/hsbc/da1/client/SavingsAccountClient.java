package com.hsbc.da1.client;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.model.Address;
import com.hsbc.da1.model.SavingsAccount;

public class SavingsAccountClient {
	public static void main(String[] args) {
		SavingsAccountController savingsAccountController = new SavingsAccountController();
		Address navinSavingAddress = new Address("street1", "city1", "state1",147001);
		Address sureshSavingAddress = new Address( "street2", "city2", "state2",157001);
		
		SavingsAccount navinSavingsAccount = savingsAccountController.openSavingAccount("Navin", 25_000, navinSavingAddress);
		SavingsAccount sureshSavingsAccount = savingsAccountController.openSavingAccount("suresh", 45_000,sureshSavingAddress);
		
		SavingsAccount rajnishSavingsAccount = savingsAccountController.openSavingAccount("rajnish", 65_000,new Address("street3", "city3", "state3",167001));
		
		System.out.println(navinSavingsAccount.getAccountBalance());
		System.out.println(sureshSavingsAccount.getAccountBalance());
		System.out.println(rajnishSavingsAccount.getAccountBalance());
	}

}
