package com.hsbc.da1.service;
import com.hsbc.da1.model.Address;
import com.hsbc.da1.model.SavingsAccount;
public interface SavingsAccountService {
	
	public SavingsAccount createSavingAccount(String customerName, double accountBalance, Address address);
	
	public void deleteSavingsAccount(long accountNumber);
	
	public SavingsAccount[] fetchSavingsAccounts();
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber);
	
	public double withdraw(long accountId, double amount);
	
	public double deposit(long accountId, double amount);
	
	public double checkBalance(long accountId);
}
