package com.hsbc.da1.service;

import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.model.Address;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.util.SavingsAccountDAOFactory;

public class SavingsAccountServiceImpl implements SavingsAccountService{
private SavingsAccountDAO dao = SavingsAccountDAOFactory.getSavingsAccountDAO();
	
	public SavingsAccount createSavingAccount(String customerName, double accountBalance, Address address) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance, address);
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		this.dao.deleteSavingsAccount(accountNumber);
	}
	
	public SavingsAccount[] fetchSavingsAccounts() {
		SavingsAccount[] accounts = this.dao.fetchSavingsAccounts();
		return accounts;
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}
	
	public double withdraw(long accountId, double amount) {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			if ( currentAccountBalance >= amount) {
				currentAccountBalance = currentAccountBalance - amount;
				savingsAccount.setAccountBalance(currentAccountBalance);
				this.dao.updateSavingsAccount(accountId, savingsAccount);
				return amount;
			}
		}
		return 0;
	}
	
	public double deposit(long accountId, double amount) {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentAccountBalance + amount);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}
	
	public double checkBalance(long accountId) {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}
}
