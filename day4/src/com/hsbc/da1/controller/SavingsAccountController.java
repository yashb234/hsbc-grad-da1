package com.hsbc.da1.controller;

import com.hsbc.da1.model.Address;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

public class SavingsAccountController {
	
	SavingsAccountService savingAccountService = SavingsAccountServiceFactory.getInstance();		

	public SavingsAccount openSavingAccount(String customerName, double accountBalance,Address address) {
		SavingsAccount savingsAccount = this.savingAccountService.createSavingAccount(customerName, accountBalance, address);
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		this.savingAccountService.deleteSavingsAccount(accountNumber);
	}
	
	public SavingsAccount[] fetchSavingsAccounts() {
		SavingsAccount[] accounts = this.savingAccountService.fetchSavingsAccounts();
		return accounts;
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) {
		SavingsAccount savingsAccount = this.savingAccountService.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}
	
	public double withdraw(long accountId, double amount) {
		return this.savingAccountService.withdraw(accountId, amount);
	}
	
	public double deposit(long accountId, double amount) {
		return this.savingAccountService.deposit(accountId, amount);
	}
	
	public double checkBalance(long accountId) {
		return this.savingAccountService.checkBalance(accountId);
	}
}
