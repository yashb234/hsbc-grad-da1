package com.hsbc.da1.model;

public class SavingsAccount {
	
	private String customerName;
	
	private long accountNumber;
	
	private double accountBalance;
	
	private Address address;
	
	private static long counter = 1000;
	
	public SavingsAccount(String customerName, double accountBalance, Address address) {
		this.customerName=customerName;
		this.accountBalance=accountBalance;
		this.accountNumber+=counter;
		this.address=address;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}
	
}
