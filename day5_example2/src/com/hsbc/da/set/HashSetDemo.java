package com.hsbc.da.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetDemo {
	public static void main(String[] args) {
		Set<Integer> set = new HashSet<>();
		set.add(23);
		set.add(23);
		set.add(23);
		set.add(23);
		set.add(23);
		set.add(33);
		set.add(44);
		set.add(55);
		
		System.out.println("Total Number of elements: "+ set.size());
		System.out.println("23");
		
		Iterator<Integer> it = set.iterator();
		
		while(it.hasNext()) {
			int value = it.next();
			System.out.println("The Value is: "+value);
			
		}
		System.out.println(set.size());
	}

}
