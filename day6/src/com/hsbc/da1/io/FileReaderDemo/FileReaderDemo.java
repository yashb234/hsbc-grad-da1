package com.hsbc.da1.io.FileReaderDemo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;

import jdk.jfr.events.FileWriteEvent;

public class FileReaderDemo extends Exception {
	public static void main(String[] args) {
		File textFile = new File("D://inFile.txt");
		File destFile= new File("D://outFile.txt");
		readFile(textFile, destFile);
	}
	
	public static void readFile(File textFile, File destFile) {
		try(
				BufferedReader bufferReader= new BufferedReader(new FileReader(textFile));
				BufferedWriter bufferWriter= new BufferedWriter(new FileWriter(destFile));
			){
			
			boolean endOfFile = false;
			while(! endOfFile) {
				String line = bufferReader.readLine();
				if(line != null) {
					bufferWriter.append(line);
					System.out.println(line);
					bufferWriter.append("\n");
				}else {
					endOfFile= true;
				}
			}
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}

}
