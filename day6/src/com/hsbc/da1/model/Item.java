package com.hsbc.da1.model;

import java.io.Serializable;

public class Item implements Serializable{
	
	private long itemId;
	private String itemName;
	private double itemPrice;
	
	public Item(long itemId, String itemName,double itemPrice) {
		this.itemId=itemId;
		this.itemName=itemName;
		this.itemPrice=itemPrice;
	}
	
	public String toString() {
		return "Item [ItemName=" + itemName + ", ItemId=" + itemId + ", ItemPrice=" + itemPrice + "]";
	}

}
