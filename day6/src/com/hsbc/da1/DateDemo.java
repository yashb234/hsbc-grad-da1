package com.hsbc.da1;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class DateDemo {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Date in dd-mm-yy format");
		String input= scanner.next();
		
		LocalDate dob = LocalDate.parse(input, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		
		LocalDate currDate = LocalDate.now();
		LocalDate futureDate = LocalDate.of(2020, 9, 28);
		
		System.out.println(Period.between(currDate, futureDate).getDays());
 	}

}
