package com.hsbc.da.threads1;

public class Client {
	
	public static void main(String[] args) {
		System.out.println(Thread.currentThread().getName());
		Runnable googlePhotos = new GooglePhotos();
		Runnable flickr = new Flickr();
		Runnable picassa = new Picassa();
		
		Thread googlePhotosThread = new Thread(googlePhotos);
		Thread flickrThread = new Thread(flickr);
		Thread picassaThread = new Thread(picassa);
		
		googlePhotosThread.start();
		flickrThread.start();
		picassaThread.start();
		
		try {
			googlePhotosThread.join();
			flickrThread.join();
			picassaThread.join();
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
