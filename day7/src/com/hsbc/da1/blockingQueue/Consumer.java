package com.hsbc.da1.blockingQueue;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

	private BlockingQueue<Message> queue;

	public Consumer(BlockingQueue<Message> queue) {
		this.queue = queue;
	}

	@Override
	public void run() {		
		consume();

	}

	private void consume() {
		boolean flag = true;
		while (flag) {
			Message message = null;
			try {
				Thread.sleep(1000);
				message = this.queue.take();
				if (message.getCommand().equals("stop")) {
					flag = false;
					System.out.println(" =========== "+message.getCommand()+" ===========");
				} else {
					System.out.println(" Message is "+ message.getCommand());
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
