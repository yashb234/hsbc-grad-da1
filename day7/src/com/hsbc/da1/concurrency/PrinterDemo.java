package com.hsbc.da1.concurrency;

public class PrinterDemo {
	public static void main(String[] args) {
		Printer printer = new Printer();
		
		Job job1= new Job(printer, 10, 15);
		Job job2= new Job(printer, 50, 60);
		Job job3= new Job(printer, 65, 70);
		
		Thread task1= new Thread(job1);
		Thread task2= new Thread(job2);
		Thread task3= new Thread(job3);
		
		task1.start();
		task2.start();
		task3.start();
		
		try {
			task1.join();
			task2.join();
			task3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Completed all the Tasks");
	}
}
class Printer{
	public synchronized void print(int start, int end) throws InterruptedException{
		if(end>=start) {
			System.out.println("======================Printing the "+start+" Page======================");
			for(int index=start; index<=end; index++) {
				System.out.printf("Printing the %d page \n", index);
				Thread.sleep(2000);
			}
		}
	}
	
}
class Job implements Runnable{
	
	private Printer printer;
	int start;
	int end;
	
	public Job(Printer printer, int start, int end) {
		this.printer=printer;
		this.start=start;
		this.end=end;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			printer.print(start, end);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}

