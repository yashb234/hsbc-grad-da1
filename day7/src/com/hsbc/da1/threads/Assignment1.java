package com.hsbc.da1.threads;

public class Assignment1 extends Thread {
	public static void main(String[] args) {
		
		System.out.println("Main Thread is starting--> "+Thread.currentThread());
		Thread t1= new Thread();
		t1.start();
		try {
			System.out.println("Inside Main Thread");
			t1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		try {
			System.out.println("Run method is running--> "+ Thread.currentThread()+" Sleep method is being invoked");
			Thread.currentThread().sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
