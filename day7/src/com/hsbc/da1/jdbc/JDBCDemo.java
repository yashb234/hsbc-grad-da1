package com.hsbc.da1.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCDemo {
	private static final String INSERT_QUERY="insert into items (name,price) values ('ipad', 33000)"
			+ "values ( 'iphone' , 29000 )";
	private static final String SELECT_QUERY="select * from items";
	public static void main(String[] args) {
		try (Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/mydb","admin","password");){
			Statement stmt = connection.createStatement();
			
			stmt.executeUpdate(INSERT_QUERY);
			
			ResultSet resultSet = stmt.executeQuery(SELECT_QUERY);			
			
			while(resultSet.next()) {
				System.out.printf("Name of mobile %s an price is %f \n", resultSet.getString(1), resultSet.getDouble(2));
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
