package com.hsbc.da1.inventory;

public class DeadLockDemo {
	private static final Message lock1=new Message("Lock1");
	private static final Message lock2=new Message("Lock2");
	
	public static void main(String[] args) {
		
		Job thread1 = new Job(lock1, lock2);
		thread1.setName("task1");
		
		Job2 thread2 = new Job2(lock1, lock2);
		thread1.setName("task2");
		
	}
}
class Job extends Thread{
	
	private Message job1;
	private Message job2;
	

	public Job(Message message1, Message message2) {
		// TODO Auto-generated constructor stub
		this.job1=message1;
		this.job2=message2;
	}
	
}
class Job2 extends Thread{

	public Job2(Message lock1, Message lock2) {
		// TODO Auto-generated constructor stub
	}
	
}

class Message{

	public Message(String string) {
		// TODO Auto-generated constructor stub
		
		
	}
	
}
