package com.hsbc.da1.client;

import com.hsbc.da1.controller.ItemController;
import com.hsbc.da1.model.Item;

public class ItemClient {
	public static void main(String[] args) {
		
		ItemController itemController = new ItemController();
		
		Item shirt = itemController.createItem("Laptop", 98_000);

		Item jeans = itemController.createItem("Mobile", 35_000);
		
		Item mobile = itemController.createItem("tablet", 58_000);

		Item[] allItems = itemController.fetchAllItems();
		
		for(Item item: allItems) {
			if(item!=null) {
				System.out.println("Item Name: "+ item.getItemName() );
				System.out.println("Item price: "+ item.getItemPrice());
				System.out.println();
			}
		}
	}

}
