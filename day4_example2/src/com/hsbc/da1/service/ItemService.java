package com.hsbc.da1.service;

import com.hsbc.da1.model.Item;

public interface ItemService {

	public Item createItem(String itemName, double itemPrice);
	
	public void deleteItem(long itemId);
	
	public Item updateNameOfItem(long itemId, String itemName);
	
	public Item updatePriceOfItem(long itemId, double itemPrice);
	
	public Item[] fetchAllItems();
	
	public Item fetchItemByItemId(long itemId);

}
