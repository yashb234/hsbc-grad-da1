package com.hsbc.da1.service;

import com.hsbc.da1.dao.ItemDAO;
import com.hsbc.da1.dao.ItemDAOimpl;
import com.hsbc.da1.model.Item;

public class ItemServiceimpl implements ItemService{
	ItemDAO dao = new ItemDAOimpl();

	public Item createItem(String itemName, double itemPrice) {
		Item item = new Item(itemName, itemPrice);
		return this.dao.storeItem(item);
	}
	
	public void deleteItem(long itemId) {
		this.dao.deleteItem(itemId);
	}
	
    public Item[] fetchAllItems() {
    	return this.dao.fetchAllItems();
    }
	
	public Item fetchItemByItemId(long itemId) {
		return this.dao.fetchItemById(itemId);
	}
	
	public Item updateNameOfItem(long itemId, String itemName) {
		Item item = this.dao.fetchItemById(itemId);
		item.setItemName(itemName);
		return this.dao.updateItem(itemId, item);
	}
	
	public Item updatePriceOfItem(long itemId, double itemPrice) {
		Item item = this.dao.fetchItemById(itemId);
		item.setItemPrice(itemPrice);
		return this.dao.updateItem(itemId, item);
	}

}
