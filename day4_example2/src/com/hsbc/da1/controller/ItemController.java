package com.hsbc.da1.controller;

import com.hsbc.da1.model.Item;
import com.hsbc.da1.service.ItemService;
import com.hsbc.da1.service.ItemServiceimpl;

public class ItemController {
	ItemService itemService = new ItemServiceimpl();
	
		public Item updatePriceOfItem(long itemId, double itemPrice) {
			return this.itemService.updatePriceOfItem(itemId, itemPrice);
		}
		
		public Item updateNameOfItem(long itemId, String itemName) {
			return this.itemService.updateNameOfItem(itemId, itemName);
		}
		
		public Item fetchItemByItemId(long itemId) {
			return this.itemService.fetchItemByItemId(itemId);
		}
		
		public Item[] fetchAllItems() {
			return this.itemService.fetchAllItems();
		}
		
		public void deleteItem(long itemId) {
			this.itemService.deleteItem(itemId);
		}
		
		public Item createItem(String itemName, double itemPrice) {
			return this.itemService.createItem(itemName, itemPrice);
		}
	

}
