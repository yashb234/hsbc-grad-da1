package com.hsbc.da1.dao;

import com.hsbc.da1.model.Item;

public interface ItemDAO {
	public Item storeItem(Item item);
	
	public Item updateItem(long itemId, Item item);
	
	public void deleteItem(long itemId);
	
	public Item[] fetchAllItems();
	
	public Item fetchItemById(long itemId);

}
