package com.hsbc.EmployMgmtSystem.util;

import com.hsbc.EmployMgmtSystem.dao.EmployeeDAO;
import com.hsbc.EmployMgmtSystem.dao.TreeSetBackedEmployeeDAOImpl;

public class EmployeeDAOFactory {
	
	public static EmployeeDAO getDaofactory() {
		
		return new TreeSetBackedEmployeeDAOImpl();
		
	}

}