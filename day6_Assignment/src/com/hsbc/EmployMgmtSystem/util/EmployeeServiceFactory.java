package com.hsbc.EmployMgmtSystem.util;

import com.hsbc.EmployMgmtSystem.service.EmployeeService;
import com.hsbc.EmployMgmtSystem.service.EmployeeServiceImpl;

public class EmployeeServiceFactory {
	
	public static EmployeeService serviceFactory() {
		return new EmployeeServiceImpl();
	}
	
}