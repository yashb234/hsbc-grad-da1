package com.hsbc.EmployMgmtSystem.client;

import com.hsbc.EmployMgmtSystem.controller.EmployeeController;
import com.hsbc.EmployMgmtSystem.exception.EmployeeNotFoundException;
import com.hsbc.EmployMgmtSystem.exception.InsufficientLeavesException;
import com.hsbc.EmployMgmtSystem.model.Employee;


public class SavingsAccountClient {
	public static void main(String[] args) throws EmployeeNotFoundException, InsufficientLeavesException {
		
	
		EmployeeController controller=new EmployeeController();
		
		Employee navinAccount= controller.createEmployeeAccount("navan", 50_000);

		Employee kamalAccount= controller.createEmployeeAccount("kamal", 60_000);
    
        
        System.out.println(navinAccount.getEmployeeID() + " "+navinAccount.getEmployeeName() + " "+ navinAccount.getSalary()+" "+navinAccount.getLeaves());
        try {
        controller.updateEmployeeleave(1002, 15);
        }catch(InsufficientLeavesException e) {
        	System.out.println("Enter Valid leaves");
        	e.getMessage();
        }
        controller.updateEmployeeSalary(1002, 65000);
        System.out.println(kamalAccount.getEmployeeID() + " "+kamalAccount.getEmployeeName() + " "+ kamalAccount.getSalary()+" "+kamalAccount.getLeaves());
 
	}

}