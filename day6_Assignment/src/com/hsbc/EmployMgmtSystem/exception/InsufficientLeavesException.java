package com.hsbc.EmployMgmtSystem.exception;

public class InsufficientLeavesException extends Exception {
	
	public InsufficientLeavesException(String message) {
		super(message);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}


}
