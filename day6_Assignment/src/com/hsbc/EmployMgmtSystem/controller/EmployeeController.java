package com.hsbc.EmployMgmtSystem.controller;

import com.hsbc.EmployMgmtSystem.model.Employee;
import com.hsbc.EmployMgmtSystem.service.EmployeeService;
import com.hsbc.EmployMgmtSystem.service.EmployeeServiceImpl;
import com.hsbc.EmployMgmtSystem.util.EmployeeServiceFactory;

import java.util.Collection;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.EmployMgmtSystem.exception.EmployeeNotFoundException;
import com.hsbc.EmployMgmtSystem.exception.InsufficientLeavesException;

public class EmployeeController {
	
	EmployeeService employeeService= EmployeeServiceFactory.serviceFactory();
	
	public Employee createEmployeeAccount(String employeeData, double salary) {
		return this.employeeService.createEmployeeAccount(employeeData, salary);
	}
	
	public Employee updateEmployeeSalary(long employeeId, double salary) throws EmployeeNotFoundException{
		return this.employeeService.updateEmployeeSalary(employeeId, salary);
	}
		
	public Employee fetchEmployeeById(long employeeId) throws EmployeeNotFoundException{
		return this.employeeService.fetchEmployeeById(employeeId);
	}
	
	public Collection<Employee> fetchAllAccounts(){
		return this.employeeService.fetchAllAccounts();
		
	}
	
	public Employee updateEmployeeleave(long employeeId, int leave) throws EmployeeNotFoundException, InsufficientLeavesException{
		
		return this.employeeService.updateEmployeeleave(employeeId, leave);
	}
	

}