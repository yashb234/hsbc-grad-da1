package com.hsbc.EmployMgmtSystem.model;

public class Employee{
	private String employeeName;
	
	private long employeeId;
	private int age;
	private double salary;
	private static long counter=1000;
	private int leaves;
	public Employee(String employeeName, double salary) {
		this.employeeName=employeeName;
		this.salary=salary;
		this.employeeId=++counter;
		this.leaves=40;
		
	}
	public int setLeaves(int leaves) {
		return this.leaves=leaves;
	}
	
	public int getLeaves() {
		return leaves;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public void setEmployeeID(long employeeId) {
		this.employeeId = employeeId;
	}

	public long getEmployeeID() {
		return employeeId;
	}
	
	public String toString() {
		return "Employee [employeeName=" + employeeName + ", employeeId=" + employeeId + ", salary="
				+ salary + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (employeeId ^ (employeeId >>> 32));
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Employee))
			return false;
		Employee other = (Employee) obj;
		if (employeeId != other.employeeId)
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		return true;
	}
	
}