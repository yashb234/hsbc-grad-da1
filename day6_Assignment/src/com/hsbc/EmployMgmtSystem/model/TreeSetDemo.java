package com.hsbc.EmployMgmtSystem.model;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {
	public static void main(String[] args) {
		Employee Deepa= new Employee("Deepa", 30_000);
		Employee Meepa= new Employee("Meepa", 40_000);
		Employee Neepa= new Employee("Neepa", 50_000);
		Employee Keepa= new Employee("Keepa", 60_000);
		
		Set<Employee> set = new TreeSet<>(new SortByAccountNameAsc());
		set.add(Deepa);
		set.add(Meepa);
		set.add(Neepa);
		set.add(Keepa);
		
		Iterator<Employee> it = set.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	} 
}

class SortByAccountNumberAsc implements Comparator<Employee>{

	@Override
	public int compare(Employee arg0, Employee arg1) {
		// TODO Auto-generated method stub
		return (int) (arg0.getEmployeeID() - arg1.getEmployeeID())  ;
	}

}
class SortByAccountNumberDsc implements Comparator<Employee>{

	@Override
	public int compare(Employee arg0, Employee arg1) {
		// TODO Auto-generated method stub
		return -1*(int) (arg0.getEmployeeID() - arg1.getEmployeeID())  ;
	}

} 

class SortByAccountNameAsc implements Comparator<Employee>{

	@Override
	public int compare(Employee arg0, Employee arg1) {
		// TODO Auto-generated method stub
		return arg0.getEmployeeName().compareTo(arg1.getEmployeeName());
	}

}
class SortByAccountNameDsc implements Comparator<Employee>{

	@Override
	public int compare(Employee arg0, Employee arg1) {
		// TODO Auto-generated method stub
		return arg1.getEmployeeName().compareTo(arg0.getEmployeeName());
	}

}