package com.hsbc.EmployMgmtSystem.dao;

import java.util.Set;

import com.hsbc.EmployMgmtSystem.exception.EmployeeNotFoundException;
import com.hsbc.EmployMgmtSystem.model.Employee;

public interface EmployeeDAO {
	
	public Employee createEmployeeAccount(Employee employee);
	
	public Set<Employee> fetchAllAccounts();
	
	public Employee fetchEmployeeDataById(long employeeId) throws EmployeeNotFoundException;
		
	public void updateEmployeeData(long employeeId,Employee employeeData);
	
	public void deleteEmployeeData(long employeeId);
	
	
}