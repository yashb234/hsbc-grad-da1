package com.hsbc.EmployMgmtSystem.dao;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.hsbc.EmployMgmtSystem.exception.EmployeeNotFoundException;
import com.hsbc.EmployMgmtSystem.model.Employee;

public class TreeSetBackedEmployeeDAOImpl implements EmployeeDAO{

	private static Set<Employee> set=new HashSet<>();
	
	@Override
	public Employee createEmployeeAccount(Employee employee) {
		set.add(employee);
		return employee;
	}

	@Override
	public Set<Employee> fetchAllAccounts() {
		// TODO Auto-generated method stub
		return set;
	}

	@Override
	public Employee fetchEmployeeDataById(long employeeId) throws EmployeeNotFoundException {
		// TODO Auto-generated method stub
		Iterator<Employee> it=set.iterator();
		
		while(it.hasNext()) {
			
			Employee entry=it.next();
			if(entry.getEmployeeID()==employeeId) {
			return entry;
			}
		}
		throw new EmployeeNotFoundException("Employee not found");
	}

	@Override
	public void updateEmployeeData(long employeeId, Employee employee) {
		// TODO Auto-generated method stub
		Iterator<Employee> it=set.iterator();
				
			while(it.hasNext()) {	
				Employee entry=it.next();
				if(entry.getEmployeeID()==employeeId) {
				entry=employee;
				}
			}
	}

	@Override
	public void deleteEmployeeData(long employeeId) {
		// TODO Auto-generated method stub
		Iterator<Employee> it=set.iterator();
		
		while(it.hasNext()) {	
			Employee entry=it.next();
			if(entry.getEmployeeID()==employeeId) {
			set.remove(entry);
			}
		}
	}

}
