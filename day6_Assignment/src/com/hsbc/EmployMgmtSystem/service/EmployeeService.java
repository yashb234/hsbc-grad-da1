package com.hsbc.EmployMgmtSystem.service;

import com.hsbc.EmployMgmtSystem.model.Employee;

import java.util.Collection;

import com.hsbc.EmployMgmtSystem.exception.EmployeeNotFoundException;
import com.hsbc.EmployMgmtSystem.exception.InsufficientLeavesException;

public interface EmployeeService {
	
	
	public Employee createEmployeeAccount(String employeeData, double salary);
	
	public Employee updateEmployeeSalary(long employeeId, double salary) throws EmployeeNotFoundException;
		
	public Employee fetchEmployeeById(long employeeId) throws EmployeeNotFoundException;
	
	public Collection<Employee> fetchAllAccounts();
	
	public Employee updateEmployeeleave(long employeeId, int leave) throws EmployeeNotFoundException, InsufficientLeavesException; 
	

	}