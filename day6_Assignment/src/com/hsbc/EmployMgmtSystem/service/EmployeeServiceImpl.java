package com.hsbc.EmployMgmtSystem.service;


import java.util.Collection;
import java.util.List;

import com.hsbc.EmployMgmtSystem.dao.TreeSetBackedEmployeeDAOImpl;
import com.hsbc.EmployMgmtSystem.dao.EmployeeDAO;
import com.hsbc.EmployMgmtSystem.exception.EmployeeNotFoundException;
import com.hsbc.EmployMgmtSystem.exception.InsufficientLeavesException;
import com.hsbc.EmployMgmtSystem.model.Employee;
import com.hsbc.EmployMgmtSystem.util.EmployeeDAOFactory;

public class EmployeeServiceImpl implements EmployeeService{
	
	EmployeeDAO dao=EmployeeDAOFactory.getDaofactory();

	@Override
	public Employee createEmployeeAccount(String employeeName, double salary) {
		// TODO Auto-generated method stub
		Employee employeeId  = new Employee(employeeName, salary);
		Employee createdEmployeeId= this.dao.createEmployeeAccount(employeeId);
		return createdEmployeeId;
	}

	@Override
	public Employee updateEmployeeSalary(long employeeId, double salary) throws EmployeeNotFoundException {
		Employee employee = this.dao.fetchEmployeeDataById(employeeId);
		if(employee!= null) {
			employee.setSalary(salary);
			this.dao.updateEmployeeData(employeeId, employee);
			return employee;
		}
		return null;
	}

	@Override
	public Employee fetchEmployeeById(long employeeId) throws EmployeeNotFoundException {
		
		return this.dao.fetchEmployeeDataById(employeeId);
	}

	@Override
	public Collection<Employee> fetchAllAccounts() {
		return this.dao.fetchAllAccounts();
	}

	@Override
	public Employee updateEmployeeleave(long employeeId, int leaves) throws EmployeeNotFoundException , InsufficientLeavesException{
		
		Employee employee = this.dao.fetchEmployeeDataById(employeeId);
		int leftLeaves=employee.getLeaves();
		if(employee!= null) {
			if(leftLeaves<=0 || leaves>10 ) {
				throw new InsufficientLeavesException("No more leaves");
			}
			if(leftLeaves-leaves>=0){
				employee.setLeaves(leftLeaves-leaves);
				this.dao.updateEmployeeData(employeeId, employee);
				return employee;
			}
			
		}
		return null;
	}
	
}